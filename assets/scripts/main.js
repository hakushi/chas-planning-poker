var PlanningDeck = function() {
    // load array with all possible card values
    this.possibleCardValues = ["0", "1/2", "1", "2", "3", "5", "8", "13", "20", "40", "100", "?", "&#8734;"];
}
PlanningDeck.prototype.init = function() {
    // remove existing click event listeners and populate card container with cards
    this.resetDeck();    
    this.loadUIEvents();    
}
PlanningDeck.prototype.loadUIEvents = function() {
    var _this = this;    
    $('#resetDeck').on('click', function() {
        // re-initialize deck
        _this.init();
        // hide button
        $(this).css('opacity', 0).removeClass('visible');
        // Repopulating the container causes a graphical bug in Firefox related to the card transformation. A small animation resets it.
        if ($.browser.mozilla) {
            $('.card').animate({width: '+=1'}, 100, function(){
                $(this).animate({width : '-=1'}, 100);
            });
        }        
    });
    // triggers when clicking on a card
    $('.flip-container .flipper').on('click', function() {
        var card = $(this).children('.card.front');
        var isFlipped = (card.hasClass('unflipped')) ? 0 : 1;

        // is card flipped? if so, flip back
        var deg = (isFlipped) ? '0deg' : '180deg';

        // hide scroll indicator
        $('.lower-section__scroll-indicator:visible').css('opacity', 0);

        // do css transition flip
        var transformArr = ['-webkit-transform', '-moz-transform', '-o-transform','-ms-transform', 'transform'];
        for(var i = 0; i < transformArr.length; i++)
            $(this).css(transformArr[i], 'rotateY('+deg+')');

        // step 1 -> 2
        if(!isFlipped) {
            // get card container and remove unflipped class
            var container = card.removeClass('unflipped').parents('.card-container');
            // get card container coordinates
            var coords = container.position();            

            // changes positioning to absolute after flipping animation is done to prevent flickering
            setTimeout(function() {
                container.css({'position' : 'absolute',  'z-index' : '2', 'left' : coords.left, 'top' : coords.top});
                    $('.card-container:has(.unflipped)').css('display', 'none');
                }, 500);
            // centers the chosen card with a css transition using offset values and hides all scroll to top of body
            setTimeout(function() {    
                _this.centerCardContainer($('.card-container'));
                $('body').animate({'scrollTop' : 0}, 700);
            },600);
                            
            // update footer instructions
            _this.updateFooterInstructions('Step 2:', 'You have made your estimation. Click on the cardback to reveal it.');
        } else {
            // step 2 -> 3
            var container = card.parents('.card-container');
            // update footer instructions
            _this.updateFooterInstructions('Done!', 'The card displayed represents your estimation. Use the "new estimation"-button to start over');
            // offsets card by reset button height in the last step if height is > 500px
            if(!container.hasClass('flippable')) {                
                var buttonOffset = ($(window).height() < 500) ? 0 : 50
                container.css('top', parseInt(container.css('top'))-buttonOffset+'px').addClass('flippable');
            }                                
            // fade in reset button
            $('#resetDeck').addClass('visible').css('opacity', 1);            
        }                            
    });
    $(window).on('orientationchange', function() {        
        _this.shouldDisplayScrollIndicator();
        // adjust centering of card container
        setTimeout(function() {
            _this.centerCardContainer($('.card-container'));            
        }, 600);        
    });
    $(document).on('scroll', function() {
        // hide scroll indicator when user scrolls        
        $('.lower-section__scroll-indicator:visible').css('opacity', 0);
    })
}
// centers the current card container
PlanningDeck.prototype.centerCardContainer = function(container) {
    var card = container.find('.card.front');    
    // get card dimensions    
    var leftOffset = card.width()/2;
    var topOffset = card.height()/2;    
    container.css({ 'transition' : '.5s all', '-moz-transition' : '.5s all', 'left' : 'calc(50% - '+leftOffset+'px)', 'top' : 'calc(50% - '+topOffset+'px)'});
}

// update footer instructions with given arguments
PlanningDeck.prototype.updateFooterInstructions = function(label, body)  {
    $(".footer__instructions-label").text(label);
    $(".footer__instructions-body").text(body);
}
// returns a html string used to print a card with its value as attribute
PlanningDeck.prototype.getCardHtml = function(value)  {
    var htmlString = "<div class=\"card-container flip-container\" ><div class=\"flipper\"><div class=\"card front unflipped\">";
    htmlString += "<div id=\"topLeftValue\" class=\"card-value left\">"+value+"</div><div id=\"centerValue\" class=\"card-value center\">"+value+"</div><div id=\"bottomRightValue\" class=\"card-value right\">"+value+"</div>";
    htmlString += "</div><div class=\"card back\"></div></div></div>";
    return htmlString;
}
// returns a html string with all possible card options using getCardHtml() 
PlanningDeck.prototype.getAllCardsHtml = function() {
    htmlString = '';
    for(var i = 0; i < this.possibleCardValues.length; i++)
        htmlString += this.getCardHtml(this.possibleCardValues[i]);
    return htmlString;
}
PlanningDeck.prototype.resetDeck = function() {
    //reset click events
    $('#resetDeck, .flip-container .flipper').off('click');
    //populate container with all cards
    $('#cardsWrapper').html(this.getAllCardsHtml());
    //populate footer instructions container
    this.updateFooterInstructions('Step 1:', 'Choose a card that represents your estimation of the time needed to complete the current task.');
    // if cards are out of the viewport, display an indication
    this.shouldDisplayScrollIndicator();
}
PlanningDeck.prototype.shouldDisplayScrollIndicator = function() {
    // show scroll indicator when cards are outside of viewport
    if($(document).height() - $(window).height() > 100)
        $('.lower-section__scroll-indicator').css('opacity', 1);
}
// create and initialize planningDeck
var deck = new PlanningDeck();
deck.init();